var PROVIDER_NONE = NewRegistrar('none');
var PROVIDER_TRANSIP = NewDnsProvider('transip', '-');

D('cafferata.dev',
    PROVIDER_NONE,
    DnsProvider(PROVIDER_TRANSIP),
    DefaultTTL('1d'),
    TXT('spf', [
        'v=spf1',
        'include:_spf.google.com',
        '-all'
    ].join(' '))
);
